<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\articulos;
use Illuminate\Support\Facades\Validator;

class ArticulosController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return articulos::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return articulos::create(['nombre' => $request['nombre'], 'descripcion' => $request['descripcion']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($data = articulos::find($id)){
            return $data;
        }else{
            return ["mensaje" => "error"];
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $articulo = articulos::find($id);
        $data = $request->all();
        $validator = Validator::make($data, [
            'nombre' => 'string',
            'descripcion' => 'string'
        ],[
            'nombre.string' => 'El nombre tiene que ser un string',
            'descripcion.String' => 'La descripcion tiene que ser un string'
        ]);
          if ($validator->fails()){
            //se guardan valores para regresarlos al formulario
            $request->flashOnly(
              ['name', 'email', 'country_id', 'state_id', 'city_id', 'type','bio']
            );  
            return back()->withErrors($validator)->with('errorRegister', 'Hubo un error');
          }
          $articulo->update(['nombre' => $data['nombre'], 'descripcion' => $data['descripcion']]);
          return $articulo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(articulos::destroy($id) == 1){
            return ["mensaje" => "Se elimino correctamente"];
        }else{
            return ["mensaje" => "No se encontro el articulo"];
        }
        
    }
}
