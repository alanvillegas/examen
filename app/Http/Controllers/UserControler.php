<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserControler extends Controller
{
    public function login(Request $request){
        $credentials = $request->only('email', 'password');
        if(Auth::attempt($credentials)){
        //if ($user = Auth::attempt(['email' => $request["email"], 'password' => $request["password"], 'active' => 1])) {
            return $credentials;
        }else{
            return ["mensaje" => "error"];
        }
    }
}
