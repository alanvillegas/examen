<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ordenes;
use App\articulos;
use App\articulosOrdenes;
use Illuminate\Support\Facades\DB;

class ordenesController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ordenes::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $articulos = [];
        $ordenes = ordenes::create(['nombre' => $request['nombre'], 'descripcion' => $request['descripcion'], "fecha" =>$request['fecha']] );
        foreach ($request["idArticulos"] as $id) {
            array_push($articulos,articulos::find($id));
            articulosOrdenes::create(["idArticulo" => $id, "idOrden" => $ordenes['id']]);
        }
        $data = ["orden" => $ordenes, "articulos" => $articulos];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('articulos_ordenes')
        ->join('ordenes', 'articulos_ordenes.idOrden', '=', 'ordenes.id')
        ->join('articulos', 'articulos_ordenes.idArticulo', '=', 'articulos.id')
        ->select('ordenes.nombre as ordenNombre', 'ordenes.descripcion as ordenDescripcion', 'ordenes.fecha',
        'articulos.nombre', 'articulos.descripcion')->where('articulos_ordenes.idOrden', $id)
        ->groupBy('ordenes.id')
        ->get();
        
        DB::table('ordenes')
            ->join('articulos_ordenes', 'ordenes.id', '=', 'articulos_ordenes.idOrden')
            ->join('articulos', 'articulos_ordenes.idArticulo', '=', 'articulos.id')
            ->select('ordenes.nombre as ordenNombre', 'ordenes.descripcion', 'ordenes.fecha',
            'articulos.nombre', 'articulos.descripcion')
            ->get();
        
        return json_encode($data);
        if($data = ordenes::find($id)){
            return $data;
        }else{
            return ["mensaje" => "error"];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orden = ordenes::destroy($id);
        if($orden == 1){
            return ["mensaje" => "Se elimino la orden"];
        }else{
            return ["mensaje" => "No se encontro la orden"];
        }
    }
}
