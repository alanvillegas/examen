<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class articulosOrdenes extends Model
{
    protected $fillable = [
        'idArticulo', 'idOrden'
    ];
}
