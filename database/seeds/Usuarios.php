<?php

use Illuminate\Database\Seeder;
use App\User;

class Usuarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $password = Hash::make('examen');

        User::create([
            'name' => 'Examen',
            'email' => 'a@a.com',
            'password' => $password,
        ]);
    }
}
