## Documentacion

El sistema corre con una version de php 7
Para levantar el sistema se tiene que situar en la carpeta raiz del proyecto, y ejecutar el 

- comando php artisan serve

Esto ejecutara el servidor y cargara el sistema. 

https://jmartindelcampo.postman.co/collections/4129029-ba278a66-defd-4321-8650-f093c61c39df?version=latest&workspace=f01a6010-fd10-4fd2-881a-36523ff29050#d20f054e-1b6e-4ff1-b708-a77ee1753e8b


## Endpoins
Api Rest
|localhost:8000| POST      | api/articulos                 | articulos.store   | App\Http\Controllers\ArticulosController@store   | api,guest    |
|localhost:8000| GET|HEAD  | api/articulos                 | articulos.index   | App\Http\Controllers\ArticulosController@index   | api,guest    |
|localhost:8000| GET|HEAD  | api/articulos/create          | articulos.create  | App\Http\Controllers\ArticulosController@create  | api,guest    |
|localhost:8000| GET|HEAD  | api/articulos/{articulo}      | articulos.show    | App\Http\Controllers\ArticulosController@show    | api,guest    |
|localhost:8000| PUT|PATCH | api/articulos/{articulo}      | articulos.update  | App\Http\Controllers\ArticulosController@update  | api,guest    |
|localhost:8000| DELETE    | api/articulos/{articulo}      | articulos.destroy | App\Http\Controllers\ArticulosController@destroy | api,guest    |
|localhost:8000| GET|HEAD  | api/articulos/{articulo}/edit | articulos.edit    | App\Http\Controllers\ArticulosController@edit    | api,guest    |
|localhost:8000| GET|HEAD  | api/ordenes                   | ordenes.index     | App\Http\Controllers\ordenesController@index     | api,guest    |
|localhost:8000| POST      | api/ordenes                   | ordenes.store     | App\Http\Controllers\ordenesController@store     | api,guest    |
|localhost:8000| GET|HEAD  | api/ordenes/create            | ordenes.create    | App\Http\Controllers\ordenesController@create    | api,guest    |
|localhost:8000| GET|HEAD  | api/ordenes/{ordene}          | ordenes.show      | App\Http\Controllers\ordenesController@show      | api,guest    |
|localhost:8000| PUT|PATCH | api/ordenes/{ordene}          | ordenes.update    | App\Http\Controllers\ordenesController@update    | api,guest    |
|localhost:8000| DELETE    | api/ordenes/{ordene}          | ordenes.destroy   | App\Http\Controllers\ordenesController@destroy   | api,guest    |
|localhost:8000| GET|HEAD  | api/ordenes/{ordene}/edit     | ordenes.edit      | App\Http\Controllers\ordenesController@edit      | api,guest    |
|localhost:8000| GET|HEAD  | api/user                      |                   | Closure                                          | api,auth:api |